
const TRIP_DATE_KEY = "tripDateKey";
const STARTING_POINT_KEY = "startingPointKey";
const DESTINATION_KEY = "destinationKey";
const STOPOVERS_KEY = "stopOversKey";
const TAXI_TYPE_KEY = "taxiTypeKey";
const TAXI_APP_DATA_KEY = "TaxiAppData";
const DISTANCE_KEY="TestDistance" ; 
const ADDRESS_KEY="addressKey";
const PRICE_KEY="TestPrice" ;
const INDEX_KEY = "IndexKey"

let localStorageArray = [];
let futureTrips = [];
let pastTrips = [];
//Class of each trip
class TaxiTrip{
    constructor(tripDate,startingPoint,destination,stopOvers,taxiType){

        this._tripDate = tripDate;
        this._startingPoint = startingPoint;
        this._destination = destination;
        this._stopOvers = stopOvers;
        this._taxiType = taxiType;  
        
    }
    
    get tripDate(){
        return  this._tripDate;
    }
    
     get startingPoint(){
        return  this._startingPoint;
    }
    
     get destination(){
        return  this._destination;
    }
    
     get stopOvers(){
        return  this._stopOvers;
    }
    
     get taxiType(){
        return  this._taxiType;
    }
  
    
     dataObject(tripDataObj){
        this._tripDate = tripDataObj._tripDate;
        this._startingPoint = tripDataObj._startingPoint;
        this._destination =  tripDataObj._destination;
        this._stopOvers = tripDataObj._stopOvers;
        this._taxiType = tripDataObj._taxiType;  
    
    }
    
   priceCalculation(totalDistance){
        let date = new Date()

        let flatPrice = 4.20;
        let distanceFare = 1.622*totalDistance;
        let vehicalRate = 1.10;
        let totalPrice;
        
        if(this._taxiType === "Sedan"){
            vehicalRate; 
        }else if(this._taxiType === "SUV"){
            vehicalRate += 3.50; 
        }else if(this._taxiType === "Van"){
            vehicalRate += 6; 
        }else if(this._taxiType === "Minibus"){
            vehicalRate += 10.00; 
        }
       
      if(date.getHours()>17 || date.getHours()<9){
          let price = flatPrice + distanceFare + vehicalRate; 
          totalPrice = 0.2*price + price;
      }else{
          totalPrice  = flatPrice + distanceFare + vehicalRate;
      }
       return totalPrice
    }
    

   
    
    fromData(userTripDataObj) {

        this._tripDate = {
        year:"",
        month:"",
        day:"",
        hours:"",
        minutes:"",
        
    }
    
    let date = new Date()
    
    let currentYear = date.getFullYear();
    
    let currentMonth = date.getMonth();
    
    let currentDay = date.getDate();
    
    let currentHour = date.getHours();
    
    let currentMinute = date.getMinutes();
        if (currentYear < this._tripDate.year) {
            if (currentMonth < this._tripDate.month) {
                if (currentDay < this._tripDate.day) {
                    if (currentHour < this._tripDate.hours) {
                        if (currentMinute < this._tripDate.minutes) {
                            

                                let data = new TaxiTrip();
                                this._futureTrips.push(tripDataObj);


                            
                        }
                    }
                }
            }
        } else {
            let data = new TaxiTrip();
            this._pastTrips.push(tripDataObj);

        }



    }
      
    
    
     
    
}

let y = getLocalStorage(TAXI_APP_DATA_KEY);
//check history or future trip
function checkStatus(y){
    if(y.length !== undefined){
    for(i =0;i<y.length;i++){
       let tripObject = y[i];
        let tripdate =y[i]._tripDate;
         let date = new Date();
        let currentYear = date.getFullYear(); 
        let currentMonth = date.getMonth();
        let currentDay = date.getDate();
  
        console.log(tripdate.substr(0,4))
        
        if (currentYear <= tripdate.substr(0,4)) {
            if (currentMonth <= tripdate.substr(5,2)) {
                if (currentDay < tripdate.substr(8,2)) {
            
                               futureTrips.push(y[i]);
  
                }else {
            pastTrips.push(y[i]);

        }
            }
        } 
        
    }
   
    }else{
        console.log("Y = undefined")
    }
}

checkStatus(y)
// go to menu page
function menuPage(){
    
    window.location = "menu.html";
}
//go to book page 
function scheduleTrip(){
    
    window.location = "book.html";
}
//Go to view all page 
function viewAllTrip(){
    
    window.location = "viewalltrip.html";
}
//Go to detail page 
function jumpToDetail(indexI){
	
	window.location = "tripdetail.html";
	
	
}	
//check local storage
function checkLocalStorage(key) {
    if (localStorage.getItem(key)) {
        return true;
    }

    return false;
}
//update local storage
function updateLocalStorage(key, data) {
    let jsonData = JSON.stringify(data);
    localStorage.setItem(key, jsonData);
}
//get local storage information
function getLocalStorage(key) {
    if (checkLocalStorage(key)) {

        let jsonData = localStorage.getItem(key);
        let data = jsonData;
        try {
            data = JSON.parse(jsonData);
        } catch (e) {
            console.error(e.message);
        } finally {
            return data;
        }
    }
    
}
//Get address from local storage
function CallAPI(lat,lng,callback){
    let  LATLNG=lat + "," + lng
  let QuerString= "https://api.opencagedata.com/geocode/v1/json?q=" + LATLNG + "&key=3cf9f1f13ea94d0f83e368880ecbfb79&jsonp=" + callback
  //console.log(QuerString)
        let script = document.createElement('script');
        script.src = QuerString;
        document.body.appendChild(script);  
    
}
//Get lat and long to get past Starting address to html
function returnPastTripStartingAddress(result){
    //console.log(result.results[0].formatted)
    document.getElementById("PastTripStartingAddress").innerHTML += result.results[0].formatted;
   

}
//Get lat and long to get past Ending address to html
function returnPastTripEndingAddress(result){
    
    document.getElementById("PastTripEndingAddress").innerHTML += result.results[0].formatted;
}
//Get lat and long to get Future Ending  address to html
function returnFutureTripEndingAddress(result){
    
     document.getElementById("FutureTripEndingAddress").innerHTML += result.results[0].formatted;
}
//Get lat and long to get Future Starting  address to html
function returnFutureTripStartingAddress(result){
    
     document.getElementById("FutureTripStartingAddress").innerText += result.results[0].formatted;
}
//Get lat and long to Starting  address to detail html
function returnDetailStartAddress(result){
	document.getElementById("starting-point").innerHTML +=  result.results[0].formatted;
}
////Get lat and long to Stop over  address to detail html
function returnDetailStopOAddress(result){
	document.getElementById("stopover-point").innerHTML +=  result.results[0].formatted +'<br>';
}
////Get lat and long to Destination  address to detail html
function returnDetailDestinationAddress(result){
	document.getElementById("destination-point").innerHTML += result.results[0].formatted;
}
// each past Trip information in View all trip page
function pastTripStatus(pastTrips){
    let pastTripDetails ="";
    let length1 = pastTrips.length;
    let length2 = futureTrips.length
    if(length1>0){
    console.log(length1)
    
     for(i=0;i<pastTrips.length;i++){
        console.log(i)
        let tripObject = pastTrips[i];
         
         pastTripDetails += `<tr><td class="mdl-data-table__cell--non-numeric">` +(i+1) +'</td>';
         pastTripDetails += '<td id=PastTripStartingAddress>' +tripObject._startingPoint +  '</td>';
         pastTripDetails += '<td id=PastTripEndingAddress>' +tripObject._destination+ '</td>';
         pastTripDetails += '<td>' + tripObject._tripDate + '</td>'
         pastTripDetails += '<td><button onclick = "deleteTrip(' + i +')"><input type="image" src="Trash%20icon.png"  width="38" height="38">'+'</button></td>'; 
         pastTripDetails += `<td><button onclick = "view(${(i + length2)})" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">Detail</button></td></tr>`; 
     }
    console.log(pastTripDetails)
        
    document.getElementById("pastTripsContent").innerHTML += pastTripDetails;    
    }else{
         pastTripDetails = 'YOU HAVE NO TRIPS IN THE PAST';
          document.getElementById("noTripPast").innerText += pastTripDetails;
    }
    
    }
// each future Trip information in View all trip page
function futureTripStatus(futuretrips){
   
    let futureTripDetails ="";
    let length1 = futuretrips.length;
    
    
    if(length1>0){
    //console.log("length"+ length1)
     for(i=0;i<futuretrips.length;i++){
      //  console.log(i)
        let tripObject = futuretrips[i];
        futureTripDetails += `<tr><td class="mdl-data-table__cell--non-numeric">` +(i+1) +'</td>';
        futureTripDetails += '<td id=FutureTripStartingAddress>'+tripObject._startingPoint + '</td>';
        futureTripDetails += '<td id=FutureTripEndingAddress>' +tripObject._destination+ '</td>';   
        futureTripDetails += '<td>' + tripObject._tripDate + '</td>'
        futureTripDetails += '<td><button onclick = "cancelTrip(' + i +')" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">Cancel'+'</button></td>'; 
		futureTripDetails += `<td><button onclick = "view(${i})" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">Detail</button></td></tr>`;
    
     }

    document.getElementById("futureTripsContent").innerHTML += futureTripDetails;
        
    }else{
         futureTripDetails = 'YOU HAVE NO TRIPS IN THE FUTURE';
          document.getElementById("noTripFuture").innerText += futureTripDetails;
    }
    }

futureTripStatus(futureTrips)

pastTripStatus(pastTrips)

console.log(futureTrips)
//console.log(pastTrips)
console.log(futureTrips)
console.log(y)
 //cancel future Trip
function cancelTrip(index){
   
    alert("Are you sure you want to cancel this trip?")
    
    console.log(futureTrips[index])
    
 
    let tripObject = futureTrips[index];
       
    futureTrips.splice(index,1); 
    document.getElementById("futureTripsContent").innerHTML = "";
    futureTripStatus(futureTrips)       
    console.log(futureTrips)
    let x = futureTrips[index]._tripDate
   for(let i =0;i<y.length;i++){
       console.log(y)
       if(y[i]._tripDate === x){
           y.splice(i,1); 
           console.log(y)
           updateLocalStorage(TAXI_APP_DATA_KEY,y)
           break;
       }
   }
   
    
    
    
    }
//delete some history trip
function deleteTrip(index){
   
    alert("Are you sure you want to delete this trip?")
    
    console.log(pastTrips[index])
    
    let tripObject = pastTrips[index];
       
    pastTrips.splice(index,1); 
    document.getElementById("pastTripsContent").innerHTML = "";
    pastTripStatus(pastTrips)       
    console.log(pastTrips)
    
     
    
    let x = pastTrips[index]._tripDate
   for(let i =0;i<y.length;i++){
       console.log(y)
       if(y[i]._tripDate === x){
           y.splice(i,1); 
           console.log(y)
           updateLocalStorage(TAXI_APP_DATA_KEY,y)
           break;
       }
   }
    }
//function for Details
function initMapFuture(tripObject) {
	
    console.log("dajisghdojaghsd")

    let st = futureTrips[index]._startingPoint[0]
    let sl = futureTrips[index]._startingPoint[1]
    let dt = futureTrips[index]._destination[0]
    let dl = futureTrips[index]._destination[1]
    let stopOs = futureTrips._stopOvers
        
    addPnt(st,sl)
	
    for(i = 0; i <= stopOs.length; i ++ ){
		
        addPnt(stopOs[i][0],stopOs[i][1])
		
        console.log(stopOs)
				//addLn()
    }
    addPnt(dt,dl)

    }
//get Index of view all trip page 
function view(tripObject) {
    window.location.href = `./tripdetail.html?tripObject=${tripObject}`
}

function testing (){
	
	let output = ""
	output = document.getElementById("tripStatus").innerHTMl
	output += "Hello"
}

            
