let markers= []
let lineLayer = null
const TripIndex = GetQueryString('tripObject');

//Get String in query 

//This is the method of obtaining data in the URL can switch some language back our normal language like %20 to space.

function GetQueryString(name) {
	
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) 
        return unescape(r[2]); return null;
    }

    let info= getLocalStorage(TAXI_APP_DATA_KEY)
    let strpoints=[]
    let despoints=[]
    let points = []
    let count=0
    for(let i=0;i < info[TripIndex]._startingPoint.length ; i++){
        strpoints.push(info[TripIndex]._startingPoint[i])
        count += 1
    }

    for(let i=0;i < info[TripIndex]._stopOvers.length ; i++){
        points.push(info[TripIndex]._stopOvers[i])
        count += 1
    }

    for(let i=0;i < info[TripIndex]._destination.length ; i++){
        despoints.push(info[TripIndex]._destination[i])
    
    }
    count += 1
    let allP = []
    allP.push(strpoints)
    for(let i=0;i < info[TripIndex]._stopOvers.length ; i++){
        allP.push(info[TripIndex]._stopOvers[i])
    
    }
    allP.push(despoints)

    let startName = ""
    startName = CallAPI(strpoints[1],strpoints[0],"returnDetailStartAddress")

    let stopOverName = []

    for (i= 0; i<points.length;i++){
        stopOverName.push(CallAPI(points[i][1],points[i][0],"returnDetailStopOAddress"))
	
    }
    CallAPI(despoints[1],despoints[0],"returnDetailDestinationAddress")
//Showing map in detail page 
function displaymap(){
			    mapboxgl.accessToken ='pk.eyJ1Ijoiamltenp6IiwiYSI6ImNrb2s0Z204ZjA2NGsydm84YjdueGhtenEifQ.vCCw-heA0b-inxYq6qZzUA';
	
				newMap = new mapboxgl.Map({
				container: 'map',
				style: 'mapbox://styles/mapbox/streets-v10',
				zoom: 12,
                center:[info[TripIndex]._startingPoint[0],info[TripIndex]._startingPoint[1]]
		})
         
}
//Add line
function addPnt(array1) {
				var marker = new mapboxgl.Marker({
						draggable: false
					})
					.setLngLat(array1)
					.addTo(newMap);
				
				markers.push(marker)

}
//Showing all information in Html     
function displayItem(points){
	for (i=0;i<points.length;i++){
		addPnt(points[i])
		
	}
}

displaymap()
displayItem(allP)
newMap.on('load', function () {

    newMap.addLayer({
        "id": "route",
        "type": "line",

        "source": {
            "type": "geojson",
            "data": {
                "type": "Feature",
                "properties": {},
                "geometry": {
                    "type": "LineString",
                    "coordinates":
                        allP
                    
                }
            }
        }
    });

});
//calculate distance by lat and long
function distanceCalculation1(allPoints){
    
  const earthRadius = 6371;


  let totalDistance=0

  for (i = 0; i < allPoints.length - 1; i++) {
   
      let longitude1 = allPoints[i][0];
     
      let longitude2 = allPoints[i + 1][0];
    
      let latitude1 = allPoints[i][1];
      let latitude2 = allPoints[i + 1][1];

      let latitudeRadians1 = latitude1 * Math.PI / 180;
      let latitudeRadians2 = latitude2 * Math.PI / 180;
      let longitudeRadians1 = longitude1 * Math.PI / 180;
      let longitudeRadians2 = longitude2 * Math.PI / 180;

      let latitudeDiff = (latitude2 - latitude1) * Math.PI / 180;
      let longitudeDiff = (longitude2 - longitude1) * Math.PI / 180;

      let a = Math.sin(latitudeDiff / 2) * Math.sin(latitudeDiff / 2) + Math.cos(latitude1) * Math.cos(latitude2) * Math.sin(longitudeDiff / 2) * Math.sin(longitudeDiff / 2);
      
      let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
      let distBetweenPoints = earthRadius * c;
     
      totalDistance += distBetweenPoints;

   
  }   
 
   return totalDistance; 
     
}

    document.getElementById("date-Point").innerText = info[TripIndex]._tripDate
    console.log(allP)
    let dis = distanceCalculation1(allP).toFixed(2)
    let newTrip = new TaxiTrip()
    let RefTaxiType=document.getElementById("taxis");
    var TaxiTypeResult = RefTaxiType.options[RefTaxiType.selectedIndex].value;
    newTrip._taxiType=TaxiTypeResult
    document.getElementById("priceArea").innerHTML ="Total Price: "+ newTrip.priceCalculation(dis).toFixed(2)
    document.getElementById("priceArea").innerHTML +="$"
    document.getElementById("date-Point").innerText = info[TripIndex]._tripDate
    document.getElementById("distanceArea").innerText ="Total Distance: "+ dis +"km"
    //update Price by taxi type
function DetailCalculate(){
    let dis = distanceCalculation1(allP).toFixed(2)
    let newTrip = new TaxiTrip()
    console
    let RefTaxiType=document.getElementById("taxis");
	  var TaxiTypeResult = RefTaxiType.options[RefTaxiType.selectedIndex].value;
	  newTrip._taxiType=TaxiTypeResult

	  document.getElementById("priceArea").innerHTML ="Total Price: "+ newTrip.priceCalculation(dis).toFixed(2)

	  document.getElementById("priceArea").innerHTML +="$"

      document.getElementById("distanceArea").innerText ="Total Distance: "+ dis +"km"

    
    
}







