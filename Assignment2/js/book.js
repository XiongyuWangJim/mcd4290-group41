let allTripsArray =[]
let addressArray =[];           
let pointLayer = null
let lineLayer = null
let points = []
let count = 0
let popupes = []
let markers = []
let address = ''
//Get current position
if('geolocation' in navigator){
	navigator.geolocation.getCurrentPosition(initMap,error)
}else {
	error()
}

function error(){
	alert("get position error")
}
//Main map function                
function initMap(location) {
    mapboxgl.accessToken ='pk.eyJ1Ijoiamltenp6IiwiYSI6ImNrb2s0Z204ZjA2NGsydm84YjdueGhtenEifQ.vCCw-heA0b-inxYq6qZzUA';

            
    map = new mapboxgl.Map({
        container: 'map',
		style: 'mapbox://styles/mapbox/streets-v10',
		zoom: 12,
        center: [location.coords.longitude, location.coords.latitude]
    })
	count += 1
	let coords = location.coords;
	points.push([coords.longitude, coords.latitude])
	addPoint([coords.longitude, coords.latitude],count)
	updateLocalStorage(STARTING_POINT_KEY,points[0])                    
	let StartPointFromStorage= getLocalStorage(STARTING_POINT_KEY)
	console.log(StartPointFromStorage[1],StartPointFromStorage[0])
	var callback="ShowStartingAddress"
    CallAPI(StartPointFromStorage[1],StartPointFromStorage[0],callback)              
  
    map.on("click", (e) => {
	count += 1               
    let point = [e.lngLat.lng, e.lngLat.lat]
	points.push(point)
	addPoint(point,count)
	updateLocalStorage(STARTING_POINT_KEY,points[0])
                                               
	if (count >= 2) {
        if (count == 2) {
                            
            
        } else {
            let point_ = []
			points.map((item, index) => {
                if (index >= 1 && index <= points.length - 2) {
									
                    point_.push(item)
                }
            })
								
			
            updateLocalStorage(STOPOVERS_KEY,point_)
			                    
        }
		
		updateLocalStorage(DESTINATION_KEY,points[points.length - 1])
		addLine()
		let DestinationPointFromStorage=getLocalStorage(DESTINATION_KEY)
		var callback ="ShowDestinationAddress" 
		CallAPI(DestinationPointFromStorage[1],DestinationPointFromStorage[0],callback)
                      
           
  
    }
    });
    }
//Output distance and price
function CalculateDistanceAndPrice(){
    alert("Are you sure to save these information?")
      //console.log(distanceCalculation(points))
    document.getElementById("Total-Distance").innerHTML ="Total Distance:"
    document.getElementById("Total-Distance").innerHTML += distanceCalculation(points).toFixed(2)
    document.getElementById("Total-Distance").innerHTML +="Km"
    let totaldistance = distanceCalculation(points)
    let newTrip = new TaxiTrip()   
    let RefTaxiType=document.getElementById("taxis");
    var TaxiTypeResult = RefTaxiType.options[RefTaxiType.selectedIndex].value;
    newTrip._taxiType=TaxiTypeResult
    document.getElementById("Total-Price").innerHTML = "Total Price:"
    document.getElementById("Total-Price").innerHTML +=newTrip.priceCalculation(totaldistance).toFixed(2)
    document.getElementById("Total-Price").innerHTML +="$"
}
//Calculate distance by lat and long
function distanceCalculation(allPoints){
     const earthRadius = 6371;
     let totalDistance=0
     for (i = 0; i < allPoints.length - 1; i++) {
         let longitude1 = allPoints[i][0];
         let longitude2 = allPoints[i + 1][0];
         let latitude1 = allPoints[i][1];
         let latitude2 = allPoints[i + 1][1];
         let latitudeRadians1 = latitude1 * Math.PI / 180;
         let latitudeRadians2 = latitude2 * Math.PI / 180;
         let longitudeRadians1 = longitude1 * Math.PI / 180;
         let longitudeRadians2 = longitude2 * Math.PI / 180;
         let latitudeDiff = (latitude2 - latitude1) * Math.PI / 180;
         let longitudeDiff = (longitude2 - longitude1) * Math.PI / 180;
         let a = Math.sin(latitudeDiff / 2) * Math.sin(latitudeDiff / 2) + Math.cos(latitude1) * Math.cos(latitude2) * Math.sin(longitudeDiff / 2) * Math.sin(longitudeDiff / 2);
         let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
         let distBetweenPoints = earthRadius * c;
         totalDistance += distBetweenPoints;
         updateLocalStorage(DISTANCE_KEY,totalDistance)
     }   
     return totalDistance; 
     
 }
//Showing points in stop over 
function ShowStopOver(){

    document.getElementById("stopover-point").innerHTML =""
    let StopoverPointFromStorage=getLocalStorage(STOPOVERS_KEY) 
    for(let i=0 ; i<StopoverPointFromStorage.length; i++){
        var callback="StopOverAddress" 
        CallAPI(StopoverPointFromStorage[i][1],StopoverPointFromStorage[i][0],callback)
    }
    }
//Using API to switch lat and long to Address               
function CallAPI(lat,lng,callback){
    let  LATLNG=lat + "," + lng
    let QuerString= "https://api.opencagedata.com/geocode/v1/json?q=" + LATLNG + "&key=3cf9f1f13ea94d0f83e368880ecbfb79&jsonp=" + callback
    let script = document.createElement('script');
    script.src = QuerString;
    document.body.appendChild(script);  
}
//Output of starting address
function ShowStartingAddress(result) {
    let output = result.results[0].formatted
    document.getElementById("starting-point").innerHTML =output
    addressArray.push(output)
    updateLocalStorage(ADDRESS_KEY,addressArray)
}
//Output of destination address
function ShowDestinationAddress(result) {
    let output2 = result.results[0].formatted
    document.getElementById("destination-point").innerHTML =output2
    addressArray.push(output2)
    updateLocalStorage(ADDRESS_KEY,addressArray)
}
//Output of stop over address
function StopOverAddress(result) {
   document.getElementById("stopover-point").innerHTML +=result.results[0].formatted + '<br>'
}
//Add marker
function addPoint(point,index) {
				var marker = new mapboxgl.Marker({
						draggable: true
                })
                .setLngLat(point)
                .addTo(map);
				marker.indexDate = index
                markers.push(marker)

                function onDragEnd(e) {
                    let index = e.target.indexDate - 1
                    var lngLat = marker.getLngLat();
					popupes[index].setLngLat(lngLat)

                    if (count >= 2) {
                        points[index] = [lngLat.lng, lngLat.lat]
						addLine()
                    }
                }
				marker.on('dragend', onDragEnd);
				addPopu(point,index)
    }
//Add line
function addLine() {
				if (lineLayer) {
					map.removeLayer('line')
					map.removeSource("line");
				}
				var line = {
					"type": "FeatureCollection",
					"features": [{
						"type": "Feature",
						"geometry": {
							"type": "LineString",
							"coordinates": points
						}
					}]
                }

				map.addSource('line', {
					"type": "geojson",
					"data": line
				});

				lineLayer = map.addLayer({
					"id": "line",
					"type": "line",
					"source": "line",
					"paint": {
						'line-color': '#0080ff',
						'line-width': 2
                    }
                });
    }
//Add popup which is remove button
function addPopu(point,index) {
				var popup = new mapboxgl.Popup({
                    closeButton: false,
                    closeOnClick: false
				});
				popupes.push(popup)
				popup.setLngLat(point)
					.setHTML("<div class='remove-btn' onclick='showInfo(" + index + ")'>remove</div>")
                    .addTo(map);
			}
//function of remove marker and update local storage			
function showInfo(index) {               
				points.splice(index - 1, 1)
				// remove
				if(markers.length>=0){
					markers.map((item,index)=>{
						item.remove()
						popupes[index].remove()
                    })
                }
                if (popupes[index - 1]) {
					popupes[index - 1].remove()
                    markers[index - 1].remove()
					points.map((item,index)=>{
                        addPoint(item,index+1)
					})
                    addLine()               
                    updateLocalStorage(STARTING_POINT_KEY,points[0])
                    let StartPointFromStorage= getLocalStorage(STARTING_POINT_KEY)
                    console.log(StartPointFromStorage[1],StartPointFromStorage[0])
                    var callback="ShowStartingAddress"  
                    
                    addressArray.push(CallAPI(StartPointFromStorage[1],StartPointFromStorage[0],callback))
                    
                    updateLocalStorage(ADDRESS_KEY,addressArray)
                }

                if (count == 2) {
					
                } else if(count > 2) {
                    let point_ = []
                    points.map((item, index) => {
                        if (index >= 1 && index <= points.length - 2) {
                            point_.push([index, item.toString()])
                        }
                    })
                    updateLocalStorage(STOPOVERS_KEY,point_)                  				
                }else{
					document.getElementById("stopover-point").innerHTML = ""
                    updateLocalStorage(DESTINATION_KEY,points[points.length - 1])
                }
                 updateLocalStorage(DESTINATION_KEY,points[points.length - 1])
                 
                 let DestinationPointFromStorage=getLocalStorage(DESTINATION_KEY)
                      
                 var callback ="ShowDestinationAddress" 
                                
                 CallAPI(DestinationPointFromStorage[1],DestinationPointFromStorage[0],callback)
                
                 updateLocalStorage(ADDRESS_KEY,addressArray)
            }
//Save all infomation in Array list 
function bookTrip(){	
    let updateDate = updateLocalStorage(TRIP_DATE_KEY,document.getElementById("dateinput").value);
    let updateType = updateLocalStorage(TAXI_TYPE_KEY,document.getElementById("taxis").value);
	let date =getLocalStorage(TRIP_DATE_KEY)
	let start =getLocalStorage(STARTING_POINT_KEY)
	let end =getLocalStorage(DESTINATION_KEY)
	let middlePoints =getLocalStorage(STOPOVERS_KEY)
	let type =getLocalStorage(TAXI_TYPE_KEY)
	let trip = new TaxiTrip(date,start,end,middlePoints,type)

	alert("Are you sure you want to book a trip with the provided informatiom?")
	
 	console.log(trip)
    allTripsArray.push(trip)
   	console.log(allTripsArray) 
  	updateLocalStorage(TAXI_APP_DATA_KEY,allTripsArray)
	
	alert("Succesfully booked trip, you can now chose to book another trip.")
	console.log(allTripsArray)
}

console.log(addressArray)








